package com.walitapp.sales.utility;


import android.content.Context;

import com.google.gson.Gson;
import com.walitapp.sales.login.model.User;

public class SharedPreferenceManager {
    public static SharedPreferenceManager yourPreference;
    public android.content.SharedPreferences sharedPreferences;

    public static SharedPreferenceManager getInstance( Context context) {
        if (yourPreference == null) {
            yourPreference = new SharedPreferenceManager(context);
        }
        return yourPreference;
    }

    public void clear() {
        sharedPreferences.edit().clear().commit();
    }

    public SharedPreferenceManager( Context context) {
        sharedPreferences = context.getSharedPreferences("Prefs", Context.MODE_PRIVATE);
    }

    public void setString(String key, String value) {
        android.content.SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putString(key, value);
        prefsEditor.commit();
    }

    public String getString(String key, String defaultValue) {
        if (sharedPreferences != null) {
            return sharedPreferences.getString(key, defaultValue);
        }
        return "";
    }

    public void setInt(String key, int value) {
        android.content.SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putInt(key, value);
        prefsEditor.commit();
    }

    public int getInt(String key, int defaultValue) {
        if (sharedPreferences != null) {
            return sharedPreferences.getInt(key, defaultValue);
        }
        return 0;
    }

    public void setBoolean(String key, boolean value) {
        android.content.SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putBoolean(key, value);
        prefsEditor.commit();
    }

    public boolean getBoolean(String key, boolean defaultValue) {
        if (sharedPreferences != null) {
            return sharedPreferences.getBoolean(key, defaultValue);
        }
        return false;
    }

    public User getUser() {
        return (User) getObject(Constant.USER, User.class);
    }


    public Object getObject(String key, Class<?> classOfT) {
        String json = getString(key);
        Object value = new Gson().fromJson(json, classOfT);
        return value;
    }

    public String getString(String key) {
        return sharedPreferences.getString(key, "");
    }

    public void putUser(String key, Object obj) {
        checkForNullKey(key);
        Gson gson = new Gson();
        putString(key, gson.toJson(obj));
    }


    public void putString(String key, String value) {
        checkForNullKey(key);
        checkForNullValue(value);
        sharedPreferences.edit().putString(key, value).apply();
    }

    public void checkForNullKey(String key) {
        if (key == null) {
            throw new NullPointerException();
        }
    }

    public void checkForNullValue(String value) {
        if (value == null) {
            throw new NullPointerException();
        }
    }

    // This four methods are used for maintaining Alarm.
   /* public void saveAlarm(ArrayList<AlarmModelArrayList> favorites) {
        android.content.SharedPreferenceManager.Editor prefsEditor = sharedPreferences.edit();
        Gson gson = new Gson();
        String jsonFavorites = gson.toJson(favorites);
        prefsEditor.putString(Constant.ALARM_LIST, jsonFavorites);
        prefsEditor.commit();
    }

    public void addAlarm(AlarmModelArrayList model) {
        ArrayList<AlarmModelArrayList> favorites = getAlarm();
        if (favorites == null)
            favorites = new ArrayList<>();
        favorites.add(model);
        saveAlarm(favorites);
    }

    public void removeAlarm(int deleteAlarmID) {
        ArrayList<AlarmModelArrayList> favorites = getAlarm();
        ArrayList<AlarmModelArrayList> newArrayList = new ArrayList<>();
        if (favorites != null) {
            for (int i = 0; i <favorites.size(); i++) {
                if(favorites.get(i).getAlarmId() != deleteAlarmID){
                    newArrayList.add(favorites.get(i));
                }
            }
            saveAlarm(newArrayList);
        }
    }

    public ArrayList<AlarmModelArrayList> getAlarm() {
        List<AlarmModelArrayList> favorites;
        if (sharedPreferences.contains(Constant.ALARM_LIST)) {
            String jsonFavorites = sharedPreferences.getString(Constant.ALARM_LIST, null);

            Gson gson = new Gson();
            AlarmModelArrayList[] favoriteItems = gson.fromJson(jsonFavorites,
                    AlarmModelArrayList[].class);

            favorites = Arrays.asList(favoriteItems);
            favorites = new ArrayList<>(favorites);
        } else
            return null;

        return (ArrayList<AlarmModelArrayList>) favorites;
    }*/
}

