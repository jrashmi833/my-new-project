package com.walitapp.sales.utility;

public class Constant {

    public static final String USER ="user";
    public static final String PREF_NAME = "pref";
    public static final String KEY_REMEMBER = "remember";
    public static final String KEY_UNAME = "username";
    public static final String KEY_PASS = "pass";
    public static  final  String IS_USER_LOGIN = "isUserLogin";
    public static final String IS_USER_LOGOUT="isUserLogout";


//    private static final String SELECT_MODULE_NAME="select module name";
//    public static final String SUBJECT = "subject";
//    public static final String CONTACT_PERSON = "contact_person";
//    public static final String CONTACT_NUMBER = "contact_name";
//    public static final String APPOINTMENT_DATE = "appointment_date";
//    public static final String APPOINTMENT_TIME = "appointment_time";
//    public static final String DESCRIPTION = "description";


}
