package com.walitapp.sales.Enquiry.add_enquiry;

import android.content.Context;

import com.walitapp.sales.Enquiry.add_enquiry.model.SalesEnquiryParameter;
import com.walitapp.sales.Enquiry.add_enquiry.model.SalesEnquiryResponse;
import com.walitapp.sales.R;
import com.walitapp.sales.dashboard.DashBoardActivity;

public class SalesEnquiryPresenter implements SalesEnquiryCallBackListner.SalesEnquiryMangerCallBack {
    public Context context;
    private SalesEnquiryCallBackListner.SalesEnquiryPresenterView salesEnquiryPresenterView;
    private SalesEnquiryManager salesEnquiryManager;


    public SalesEnquiryPresenter( Context context, SalesEnquiryCallBackListner.SalesEnquiryPresenterView salesEnquiryPresenterView  ) {
        this.context = context;
        this.salesEnquiryPresenterView =  salesEnquiryPresenterView;
        this.salesEnquiryManager = new SalesEnquiryManager(context,this );
    }

    public  void callAddSaleEnquiryApi( SalesEnquiryParameter salesEnquiryParameter ){
        if (((DashBoardActivity) context).isInternetConneted()) {
            salesEnquiryPresenterView.onShowLoader();
            salesEnquiryManager.callAddSaleEnquiryApi(salesEnquiryParameter);
        } else {
            salesEnquiryPresenterView.onError(context.getString( R.string.no_internet_connection ) );
        }}




    @Override
    public void onSuccessSalesEnquiry( SalesEnquiryResponse salesEnquiryResponse ) {
        salesEnquiryPresenterView.onHideLoader();
        salesEnquiryPresenterView.onSuccessSalesEnquiry( salesEnquiryResponse );
    }

    @Override
    public void onError( String errorMessage ) {
        salesEnquiryPresenterView.onHideLoader();
        salesEnquiryPresenterView.onError( errorMessage );
    }


    @Override
    public void onTokenChangeError( String message ) {
        salesEnquiryPresenterView.onHideLoader();
        salesEnquiryPresenterView.onTokenChangeError( message );
    }
}


