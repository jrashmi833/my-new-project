package com.walitapp.sales.Enquiry;

import com.walitapp.sales.Enquiry.model.EnquiryResponse;

public interface EnquiryManagerCallBack {
    void onSuccessEnquiryList( EnquiryResponse enquiryResponse );
    void onError( String errorMessage );
    void  onTokenChangeError(String message);
}
