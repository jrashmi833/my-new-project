package com.walitapp.sales.Enquiry.add_enquiry;

import android.content.Context;

import com.walitapp.sales.Enquiry.add_enquiry.model.SalesEnquiryParameter;
import com.walitapp.sales.Enquiry.add_enquiry.model.SalesEnquiryResponse;
import com.walitapp.sales.R;
import com.walitapp.sales.basic.Api;
import com.walitapp.sales.login.model.LoginParameter;
import com.walitapp.sales.rest.ServiceGenerator;
import com.walitapp.sales.utility.SharedPreferenceManager;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SalesEnquiryManager {
    private SalesEnquiryCallBackListner.SalesEnquiryMangerCallBack salesEnquiryMangerCallBack;
    public Context context;

    public SalesEnquiryManager( Context context, SalesEnquiryCallBackListner.SalesEnquiryMangerCallBack salesEnquiryMangerCallBack ) {
        this.salesEnquiryMangerCallBack = salesEnquiryMangerCallBack;
        this.context = context;
    }

    public void callAddSaleEnquiryApi(SalesEnquiryParameter salesEnquiryParameter) {
        Api api = ServiceGenerator.createService( Api.class );
        String accessToken = SharedPreferenceManager.getInstance( context ).getUser().getAccessToken();
        Call<SalesEnquiryResponse> salesEnquiryResponseCall = api.callAddSaleEnquiryApi(accessToken,salesEnquiryParameter);
        salesEnquiryResponseCall.enqueue( new Callback<SalesEnquiryResponse>() {
            @Override
            public void onResponse( Call<SalesEnquiryResponse> call, Response<SalesEnquiryResponse> response ) {
                if (null != response.body()) {
                    if (response.body().getStatus().equals( "success" )) {
                        salesEnquiryMangerCallBack.onSuccessSalesEnquiry( response.body() );
                    } else {
                        if(response.body().getCode()== 400){
                            salesEnquiryMangerCallBack.onTokenChangeError( response.body().getMessage() );

                        }else {
                            salesEnquiryMangerCallBack.onError( response.body().getMessage() );
                        }
                    }

                }else {
                    salesEnquiryMangerCallBack.onError( context.getString( R.string.opps_something_went_wrong ) );
                }
            }

            @Override
            public void onFailure( Call<SalesEnquiryResponse> call, Throwable t ) {
                if(t instanceof IOException){
                    salesEnquiryMangerCallBack.onError("server_down");
                }else {
                    salesEnquiryMangerCallBack.onError("opps_something_went_wrong");
                }
            }
        } );
    }


}
