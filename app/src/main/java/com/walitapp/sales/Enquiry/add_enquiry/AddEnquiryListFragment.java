package com.walitapp.sales.Enquiry.add_enquiry;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.walitapp.sales.Enquiry.add_enquiry.model.SalesEnquiryParameter;
import com.walitapp.sales.Enquiry.add_enquiry.model.SalesEnquiryResponse;
import com.walitapp.sales.R;
import com.walitapp.sales.dashboard.DashBoardActivity;
import com.walitapp.sales.login.LoginActivity;
import com.walitapp.sales.utility.Constant;
import com.walitapp.sales.utility.SharedPreferenceManager;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class AddEnquiryListFragment extends Fragment implements DatePickerDialog.OnDateSetListener, SalesEnquiryCallBackListner.SalesEnquiryPresenterView {
    public static final String TAG = AddEnquiryListFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.spinner1)
    Spinner spinner1;
    @BindView(R.id.etSubject)
    EditText etSubject;
    @BindView(R.id.etContactPerson)
    EditText etContactPerson;
    @BindView(R.id.etContactName)
    EditText etContactName;
    @BindView(R.id.etAppointmentDate)
    TextView etAppointmentDate;
    @BindView(R.id.ivDate)
    ImageView ivDate;
    @BindView(R.id.etAppointmentTime)
    TextView etAppointmentTime;
    @BindView(R.id.iVTime)
    ImageView iVTime;
    @BindView(R.id.etDescription)
    EditText etDescription;
    Unbinder unbinder;
    @BindView(R.id.buttonSubmit)
    Button buttonSubmit;
    @BindView(R.id.datePiker)
    LinearLayout datePiker;
    @BindView(R.id.timePiker)
    LinearLayout timePiker;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;
    String ampm = "";
    private String format = "";
    String moduleName = "";
    ArrayList<String> categories;

    Calendar calendar;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    public SalesEnquiryPresenter salesEnquiryPresenter;
    private SalesEnquiryParameter loginParameter;


    public AddEnquiryListFragment() {

    }


    public static AddEnquiryListFragment newInstance( String param1, String param2 ) {
        AddEnquiryListFragment fragment = new AddEnquiryListFragment();
        Bundle args = new Bundle();
        args.putString( ARG_PARAM1, param1 );
        args.putString( ARG_PARAM2, param2 );
        fragment.setArguments( args );
        return fragment;
    }

    @Override
    public void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        if (getArguments() != null) {
            mParam1 = getArguments().getString( ARG_PARAM1 );
            mParam2 = getArguments().getString( ARG_PARAM2 );
        }
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState ) {
        view = inflater.inflate( R.layout.fragment_add_enquiry_list2, container, false );
        unbinder = ButterKnife.bind( this, view );

        salesEnquiryPresenter = new SalesEnquiryPresenter( context, this );

        setModuleAdapter();
        return view;
    }

    private void setModuleAdapter() {
        categories = new ArrayList<>();
        categories.add( 0, "Select Module Name" );
        categories.add( "Payit" );
        categories.add( "Shopit" );
        categories.add( "Helpit" );
        categories.add( "Bookit" );
        categories.add( "Food And Drink" );
        categories.add( "Food Driver" );
        categories.add( "Taxi Driver" );
        categories.add( "Others" );

        ArrayAdapter dataAdapter = new ArrayAdapter( getActivity(), android.R.layout.simple_spinner_item, categories );
        dataAdapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );
        spinner1.setAdapter( dataAdapter );
        spinner1.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected( AdapterView<?> parent, View view, int position, long id ) {
                if (parent.getItemAtPosition( position ).equals( "Select Module Name" )) {
                    moduleName = "";
                } else {
                    //On selecting spinner item
                    String item = parent.getItemAtPosition( position ).toString();
                    moduleName = item;

                }
            }

            @Override
            public void onNothingSelected( AdapterView<?> parent ) {

            }
        } );

    }

    @Override
    public void onAttach( Context context ) {
        super.onAttach( context );
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    @OnClick({R.id.timePiker, R.id.datePiker, R.id.buttonSubmit})
    public void onViewClicked( View view ) {
        switch (view.getId()) {
            case R.id.datePiker:
                openDatePickerDialog();
                break;
            case R.id.timePiker:
                openTimePickerDilog();
                break;
            case R.id.buttonSubmit:
                if (validate()) {
                    SalesEnquiryParameter salesEnquiryParameter = new SalesEnquiryParameter();
                    salesEnquiryParameter.setModuleName( moduleName );
                    salesEnquiryParameter.setSubject( etSubject.getText().toString().trim() );
                    salesEnquiryParameter.setContactPerson( etContactPerson.getText().toString().trim() );
                    salesEnquiryParameter.setContactNumber( etContactName.getText().toString().trim() );
                    salesEnquiryParameter.setAppointmentDate( etAppointmentDate.getText().toString().trim() );
                    salesEnquiryParameter.setAppointmentTime( etAppointmentTime.getText().toString().trim() );
                    salesEnquiryParameter.setDescription( etDescription.getText().toString().trim() );
                    salesEnquiryPresenter.callAddSaleEnquiryApi( salesEnquiryParameter );
                }
                break;
        }
    }


    private void openTimePickerDilog() {
        calendar = Calendar.getInstance();
        int currentHour = calendar.get( Calendar.HOUR_OF_DAY );
        int currentMinute = calendar.get( Calendar.MINUTE );
        TimePickerDialog timePickerDialog = new TimePickerDialog( context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet( TimePicker view, int hourOfDay, int minute ) {
                if (hourOfDay >= 12) {
                    ampm = "PM";
                } else {
                    ampm = "AM";
                }
//                if (hourOfDay == 0) {
//                    hourOfDay += 12;
//                    format = "AM";
//                } else if (hourOfDay == 12) {
//                    format = "PM";
//                } else if (hourOfDay > 12) {
//                    hourOfDay -= 12;
//                    format = "PM";
//                } else {
//                    format = "AM";
//                }
                etAppointmentTime.setText( String.format( "%2d:%2d", hourOfDay, minute ) + ampm );
//                etAppointmentTime.setText( hourOfDay + ":" + minute );
            }
        }, currentHour, currentMinute, false );
        timePickerDialog.show();
    }


    private void openDatePickerDialog() {
        calendar = Calendar.getInstance();
        int day = calendar.get( Calendar.DAY_OF_MONTH );
        int month = calendar.get( Calendar.MONTH );
        int year = calendar.get( Calendar.YEAR );
        DatePickerDialog datePickerDialog = new DatePickerDialog( context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet( DatePicker view, int mYear, int mMonth, int mday ) {
                etAppointmentDate.setText( mday + "/" + (mMonth + 1) + "/" + mYear );
            }
        }, year, month, day );
        datePickerDialog.show();
    }

    @Override
    public void onDateSet( DatePicker view, int year, int month, int dayOfMonth ) {

    }

    @Override
    public void onSuccessSalesEnquiry( SalesEnquiryResponse salesEnquiryResponse ) {
        ((DashBoardActivity) context).showToast( salesEnquiryResponse.getMessage() );
        getActivity().onBackPressed();
    }

    @Override
    public void onError( String errorMessage ) {
        ((DashBoardActivity) context).showToast( errorMessage );
    }

    @Override
    public void onShowLoader() {
        ((DashBoardActivity) context).showLoader();

    }

    @Override
    public void onTokenChangeError( String message ) {
        ((DashBoardActivity) context).showToast( message );
        SharedPreferenceManager.getInstance( context ).putUser( Constant.USER, null );
        Intent intent = new Intent( context, LoginActivity.class );
        intent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
        startActivity( intent );
        getActivity().onBackPressed();
    }

    @Override
    public void onHideLoader() {
        ((DashBoardActivity) context).hideLoader();
    }

    public Boolean validate() {
        if (spinner1.getSelectedItemPosition() == 0 || moduleName.equalsIgnoreCase( "" )) {
            Toast.makeText( context, "please select module", Toast.LENGTH_LONG ).show();
            return false;
        } else if (etSubject.getText().toString().trim().length() == 0) {
            Toast.makeText( context, "Please Subject", Toast.LENGTH_LONG ).show();
            return false;
        } else if (etContactPerson.getText().toString().trim().length() == 0) {
            Toast.makeText( context, "Please  enter the contact person name", Toast.LENGTH_LONG ).show();
            return false;
        } else if (etContactName.getText().toString().trim().length() == 0) {
            Toast.makeText( context, "Please enter the number ", Toast.LENGTH_LONG ).show();
            return false;
        } else if (etAppointmentDate.getText().toString().trim().length() == 0) {
            Toast.makeText( context, "Please Enter Appointment Date ", Toast.LENGTH_LONG ).show();
            return false;
        } else if (etAppointmentTime.getText().toString().trim().length() == 0) {
            Toast.makeText( context, "Please Enter Appointment Time", Toast.LENGTH_LONG ).show();
            return false;
        }

        return true;
    }
}


