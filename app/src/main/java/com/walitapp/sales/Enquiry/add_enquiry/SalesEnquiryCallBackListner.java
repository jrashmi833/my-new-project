package com.walitapp.sales.Enquiry.add_enquiry;

import com.walitapp.sales.Enquiry.add_enquiry.model.SalesEnquiryResponse;

public interface SalesEnquiryCallBackListner {
    public interface SalesEnquiryPresenterView {
        void onSuccessSalesEnquiry( SalesEnquiryResponse salesEnquiryResponse );
        void onError( String errorMessage );
        void onShowLoader();
        void onTokenChangeError(String message);
        void onHideLoader();
    }

    public interface SalesEnquiryMangerCallBack {
        void onSuccessSalesEnquiry( SalesEnquiryResponse salesEnquiryResponse );
        void onError( String errorMessage );
        void onTokenChangeError( String message);
    }
}
