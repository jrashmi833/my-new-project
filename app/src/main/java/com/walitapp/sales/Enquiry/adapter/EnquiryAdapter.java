package com.walitapp.sales.Enquiry.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.walitapp.sales.R;
import com.walitapp.sales.Enquiry.model.EnquiryArrayList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EnquiryAdapter extends RecyclerView.Adapter<EnquiryAdapter.ViewHolder> {

    private Context context;
    private ArrayList<EnquiryArrayList> enquiryArrayLists;

    public EnquiryAdapter( Context context, ArrayList<EnquiryArrayList> enquiryArrayLists ) {
        this.context = context;
        this.enquiryArrayLists = enquiryArrayLists;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder( @NonNull ViewGroup parent, int i ) {
        View view = LayoutInflater.from( context ).inflate( R.layout.row_enquirylistview, parent, false );
        return new ViewHolder( view );
    }

    @Override
    public void onBindViewHolder( @NonNull ViewHolder holder, int position ) {
    holder.textViewModuleName.setText( enquiryArrayLists.get( position ).getModuleName() );
        holder.textViewSubject.setText( enquiryArrayLists.get( position ).getSubject() );
        holder.textViewDescription.setText( enquiryArrayLists.get( position ).getDescription() );
        holder.textViewContactPerson.setText( enquiryArrayLists.get( position ).getContactPerson() );
        holder.textViewContactNumber.setText( enquiryArrayLists.get( position ).getContactNumber() );
        holder.textViewAppoitmentDate.setText( enquiryArrayLists.get( position ).getAppointmentDate() );
        holder.textViewAppoitmentTime.setText( enquiryArrayLists.get( position ).getAppointmentTime() );
        holder.textViewCreatedAt.setText( enquiryArrayLists.get( position ).getCreatedAt() );

    }

    @Override
    public int getItemCount() {
        return enquiryArrayLists.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewModuleName)
        TextView textViewModuleName;
        @BindView(R.id.textViewSubject)
        TextView textViewSubject;
        @BindView(R.id.textViewDescription)
        TextView textViewDescription;
        @BindView(R.id.textViewContactPerson)
        TextView textViewContactPerson;
        @BindView(R.id.textViewContactNumber)
        TextView textViewContactNumber;
        @BindView(R.id.textViewAppoitmentDate)
        TextView textViewAppoitmentDate;
        @BindView(R.id.textViewAppoitmentTime)
        TextView textViewAppoitmentTime;
        @BindView(R.id.textViewCreatedAt)
        TextView textViewCreatedAt;
        public ViewHolder( @NonNull View itemView ) {
            super( itemView );
            ButterKnife.bind( this,itemView );
        }
    }
}
