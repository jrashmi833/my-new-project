package com.walitapp.sales.Enquiry;

import android.content.Context;

import com.walitapp.sales.R;
import com.walitapp.sales.basic.Api;
import com.walitapp.sales.Enquiry.model.EnquiryResponse;
import com.walitapp.sales.rest.ServiceGenerator;
import com.walitapp.sales.utility.SharedPreferenceManager;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EnquiryManager {
    private Context context;
    private EnquiryManagerCallBack enquiryManagerCallBack;

    public EnquiryManager( Context context, EnquiryManagerCallBack enquiryManagerCallBack ) {
        this.context = context;
        this.enquiryManagerCallBack = enquiryManagerCallBack;
    }

    public void callGetEnquiryListApi() {
        Api api = ServiceGenerator.createService( Api.class );
        String accessToken = SharedPreferenceManager.getInstance( context ).getUser().getAccessToken();
        Call<EnquiryResponse> cityResponseCall = api.callGetEnquiryListApi(accessToken);
        cityResponseCall.enqueue( new Callback<EnquiryResponse>() {
            @Override
            public void onResponse( Call<EnquiryResponse> call, Response<EnquiryResponse> response ) {
                if (null != response.body()) {
                    if (response.body().getStatus().equals( "success" )) {
                        enquiryManagerCallBack.onSuccessEnquiryList( response.body() );
                    } else {
                        if(response.body().getCode()== 400){
                            enquiryManagerCallBack.onTokenChangeError( response.body().getMessage() );

                        }else {
                            enquiryManagerCallBack.onError( response.body().getMessage() );
                        }
                    }

                }else {
                    enquiryManagerCallBack.onError( context.getString( R.string.opps_something_went_wrong ) );
                }
            }

            @Override
            public void onFailure( Call<EnquiryResponse> call, Throwable t ) {
                if (t instanceof IOException) {
                    enquiryManagerCallBack.onError( context.getString( R.string.server_down ) );
                } else {
                    enquiryManagerCallBack.onError( context.getString( R.string.opps_something_went_wrong ) );
                }
            }
        } );
    }

}