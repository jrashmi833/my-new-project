package com.walitapp.sales.Enquiry;

import com.walitapp.sales.Enquiry.model.EnquiryResponse;

public interface EnquiryPresenterView {
    void onSuccessList( EnquiryResponse enquiryResponse );
    void onError( String errorMessage );
    void  onTokenChangeError(String message);
    void onShowLoader();
    void onHideLoader();
}

