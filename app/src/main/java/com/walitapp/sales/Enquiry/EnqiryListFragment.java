package com.walitapp.sales.Enquiry;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.walitapp.sales.Enquiry.add_enquiry.AddEnquiryListFragment;
import com.walitapp.sales.Enquiry.adapter.EnquiryAdapter;
import com.walitapp.sales.R;
import com.walitapp.sales.Enquiry.model.EnquiryArrayList;
import com.walitapp.sales.Enquiry.model.EnquiryResponse;
import com.walitapp.sales.dashboard.DashBoardActivity;
import com.walitapp.sales.login.LoginActivity;
import com.walitapp.sales.utility.Constant;
import com.walitapp.sales.utility.SharedPreferenceManager;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class EnqiryListFragment extends Fragment implements EnquiryPresenterView {
    public static final String TAG = EnqiryListFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.imageViewBack)
    ImageView imageViewBack;
    @BindView(R.id.imageViewAdd)
    ImageView imageViewAdd;
    Unbinder unbinder;
    @BindView(R.id.recycleViewArrayList)
    RecyclerView recycleViewArrayList;
    private String mParam1;
    private String mParam2;
    private Context context;
    private View view;
    public EnquiryPresenter enquiryPresenter;
    public ArrayList<EnquiryArrayList> enquiryArrayListArrayList;

    public EnqiryListFragment() {
        // Required empty public constructor
    }

    public static EnqiryListFragment newInstance( String param1, String param2 ) {
        EnqiryListFragment fragment = new EnqiryListFragment();
        Bundle args = new Bundle();
        args.putString( ARG_PARAM1, param1 );
        args.putString( ARG_PARAM2, param2 );
        fragment.setArguments( args );
        return fragment;
    }

    @Override
    public void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        if (getArguments() != null) {
            mParam1 = getArguments().getString( ARG_PARAM1 );
            mParam2 = getArguments().getString( ARG_PARAM2 );
        }
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState ) {
        // Inflate the layout for this fragment
        view = inflater.inflate( R.layout.fragment_enqiry_list, container, false );
        unbinder = ButterKnife.bind( this, view );
        enquiryPresenter = new EnquiryPresenter( context, this );
        enquiryPresenter.callGetEnquiryListApi( );
        return view;
    }

    @Override
    public void onAttach( Context context ) {
        super.onAttach( context );
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void replaceFragmentHomeFragment( Fragment fragment, String tag, boolean isAddToBackStack ) {
        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace( R.id.relativeLayoutFragmentMainContainer, fragment, tag );
        if (isAddToBackStack) {
            fragmentTransaction.addToBackStack( tag );
        }

        fragmentTransaction.commit();
    }

    @OnClick({R.id.imageViewBack, R.id.imageViewAdd})
    public void onViewClicked( View view ) {
        switch (view.getId()) {
            case R.id.imageViewBack:
                getActivity().onBackPressed();
                break;
            case R.id.imageViewAdd:
                replaceFragmentHomeFragment( new AddEnquiryListFragment(), AddEnquiryListFragment.TAG, true );
                break;
        }
    }

    private void setDataAdapter() {
        EnquiryAdapter enquiryAdapter = new EnquiryAdapter( context, enquiryArrayListArrayList );
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager( context, LinearLayoutManager.VERTICAL, false );
        if (null != recycleViewArrayList) {
            recycleViewArrayList.setLayoutManager( layoutManager );
            recycleViewArrayList.setAdapter( enquiryAdapter );
        }
    }

    @Override
    public void onSuccessList( EnquiryResponse enquiryResponse ) {
        enquiryArrayListArrayList = new ArrayList<>();
        enquiryArrayListArrayList.addAll( enquiryResponse.getData() );
        ((DashBoardActivity)context).showToast( enquiryResponse.getMessage() );
        setDataAdapter();
    }

    @Override
    public void onError( String errorMessage ) {
        ((DashBoardActivity)context).showToast(errorMessage);
    }

    @Override
    public void onTokenChangeError( String message ) {
        ((DashBoardActivity)context).showToast(message );
        SharedPreferenceManager.getInstance( context ).putUser( Constant.USER,null);
        SharedPreferenceManager.getInstance( context ).setBoolean( Constant.IS_USER_LOGIN,false);
        Intent intent = new Intent(context, LoginActivity.class );
        intent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
        startActivity( intent );
        getActivity().finish();
    }

    @Override
    public void onShowLoader() {
        ((DashBoardActivity)context).showLoader();
    }

    @Override
    public void onHideLoader() {
        ((DashBoardActivity)context).hideLoader();

    }
}
