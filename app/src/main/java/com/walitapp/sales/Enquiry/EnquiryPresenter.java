package com.walitapp.sales.Enquiry;

import android.content.Context;

import com.walitapp.sales.dashboard.DashBoardActivity;
import com.walitapp.sales.Enquiry.model.EnquiryResponse;
import com.walitapp.sales.R;

public class EnquiryPresenter implements EnquiryManagerCallBack {
    public Context context;
    private EnquiryPresenterView enquiryPresenterView;
    private EnquiryManager enquiryManager;


    public EnquiryPresenter( Context context, EnquiryPresenterView enquiryPresenterView ) {
        this.context = context;
        this.enquiryPresenterView = enquiryPresenterView;
        this.enquiryManager = new EnquiryManager( context,this );


    }
    public void callGetEnquiryListApi(){
        if (((DashBoardActivity) context).isInternetConneted()) {
            enquiryPresenterView.onShowLoader();
            enquiryManager.callGetEnquiryListApi();
        } else {
            enquiryPresenterView.onError(context.getString( R.string.no_internet_connection ) );
        }}
    @Override
    public void onSuccessEnquiryList( EnquiryResponse enquiryResponse ) {
        enquiryPresenterView.onHideLoader();
        enquiryPresenterView.onSuccessList( enquiryResponse );
    }

    @Override
    public void onError( String errorMessage ) {
        enquiryPresenterView.onHideLoader();
        enquiryPresenterView.onError( errorMessage );

    }

    @Override
    public void onTokenChangeError( String message ) {
        enquiryPresenterView.onHideLoader();
        enquiryPresenterView.onTokenChangeError( message );
    }
}
