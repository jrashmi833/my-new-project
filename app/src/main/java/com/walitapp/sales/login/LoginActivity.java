package com.walitapp.sales.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.walitapp.sales.dashboard.DashBoardActivity;
import com.walitapp.sales.R;
import com.walitapp.sales.basic.BaseActivity;
import com.walitapp.sales.login.model.LoginParameter;
import com.walitapp.sales.login.model.LoginResponse;
import com.walitapp.sales.utility.Constant;
import com.walitapp.sales.utility.SharedPreferenceManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity implements LoginPresenterView {

    @BindView(R.id.etEmail)
    EditText etEmail;
    @BindView(R.id.etPassword)
    EditText etPassword;
    @BindView(R.id.iconPassword)
    ImageView iconPassword;
    @BindView(R.id.chekboxRememberMe)
    CheckBox chekboxRememberMe;
    @BindView(R.id.btLogin)
    Button btLogin;
    public Context context = this;
    public LoginPresenter loginPresenter;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    private  boolean isPasswordShown = false;


    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_login );
        ButterKnife.bind( this );
        loginPresenter = new LoginPresenter( context, this );
        sharedPreferences = getSharedPreferences( Constant.USER, Context.MODE_PRIVATE );
        editor = sharedPreferences.edit();

        if (sharedPreferences.getBoolean( Constant.KEY_REMEMBER, false )) {
            chekboxRememberMe.setChecked( true );
            etEmail.setText( sharedPreferences.getString( Constant.KEY_UNAME, "" ) );
            etPassword.setText( sharedPreferences.getString( Constant.KEY_PASS, "" ) );
        } else {
            chekboxRememberMe.setChecked( false );
        }

    }


    public boolean validate() {
        if (etEmail.getText().toString().trim().length() == 0) {
            Toast.makeText( this, "Please enter user name", Toast.LENGTH_LONG ).show();
            return false;
        } else if (etPassword.getText().toString().trim().length() == 0) {
            Toast.makeText( this, "Please enter password", Toast.LENGTH_LONG ).show();
            return false;
        } else if (etPassword.getText().toString().trim().length() < 6) {
            Toast.makeText( this, "Please contain atleast six digits", Toast.LENGTH_LONG ).show();
            return false;
        }
        return true;

    }

    @Override
    public void onSucessLogin( LoginResponse loginResponse ) {
        showToast( loginResponse.getMessage() );
        managepref();
        SharedPreferenceManager.getInstance( context ).putUser( Constant.USER, loginResponse.getData() );
        SharedPreferenceManager.getInstance( context ).setBoolean( Constant.IS_USER_LOGIN, true );
        Intent intent = new Intent( this, DashBoardActivity.class );
        startActivity( intent );
        finish();

    }

    @Override
    public void onError( String errrorMessage ) {
        showToast( errrorMessage );
    }

    @Override
    public void onShowloader() {
        showLoader();
    }

    @Override
    public void onhideLoader() {
        hideLoader();
    }


    private void managepref() {
        if (chekboxRememberMe.isChecked()) {
            editor.putString( Constant.KEY_UNAME, etEmail.getText().toString().trim() );
            editor.putString( Constant.KEY_PASS, etPassword.getText().toString().trim() );
            editor.putBoolean( Constant.KEY_REMEMBER, true );
            editor.apply();
            editor.commit();
        } else {
            editor.putBoolean( Constant.KEY_REMEMBER, false );
            editor.remove( Constant.KEY_UNAME );
            editor.remove( Constant.KEY_PASS );
            editor.apply();

        }
    }

    @OnClick({R.id.iconPassword, R.id.btLogin})
    public void onViewClicked( View view ) {
        switch (view.getId()) {
            case R.id.iconPassword:
                if(isPasswordShown){
                    isPasswordShown = false ;
                    iconPassword.setImageResource(R.drawable.icon_view_password );
                    etPassword.setTransformationMethod( PasswordTransformationMethod.getInstance());
                    if (etPassword.getText().toString().trim().length() != 0) {
                        etPassword.setSelection(etPassword.getText().length());
                    }
                }else {
                    iconPassword.setImageResource(R.drawable.icon_hide_password );
                    isPasswordShown = true;
                    etPassword.setTransformationMethod( HideReturnsTransformationMethod.getInstance());
                    if (etPassword.getText().toString().trim().length() != 0) {
                        etPassword.setSelection(etPassword.getText().length());
                    }
                }

                break;
            case R.id.btLogin:
                if (validate()) {
                    LoginParameter loginParameter = new LoginParameter();
                    loginParameter.setPassword( etPassword.getText().toString().trim() );
                    loginParameter.setUsername( etEmail.getText().toString().trim() );
                    loginPresenter.callLoginApi( loginParameter );
                }
                break;
        }
    }
}
