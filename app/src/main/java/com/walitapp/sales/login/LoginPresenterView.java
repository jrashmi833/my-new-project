package com.walitapp.sales.login;

import com.walitapp.sales.login.model.LoginResponse;

public interface LoginPresenterView {

    void onSucessLogin( LoginResponse loginResponse );

    void onError( String errrorMessage );

    void onShowloader();

    void onhideLoader();

}
