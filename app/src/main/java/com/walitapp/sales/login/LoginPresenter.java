package com.walitapp.sales.login;
import android.content.Context;

import com.walitapp.sales.R;
import com.walitapp.sales.login.LoginActivity;
import com.walitapp.sales.login.LoginManager;
import com.walitapp.sales.login.LoginPresenterView;
import com.walitapp.sales.login.model.LoginParameter;
import com.walitapp.sales.login.model.LoginResponse;

public class LoginPresenter implements LoginManagerCallback {
    public LoginManager loginManager;
    public Context context;
    public LoginPresenterView loginPresenterView;

    public LoginPresenter(Context context, LoginPresenterView loginPresenterView ) {
        this.loginManager = new LoginManager( context,this );
        this.context = context;
        this.loginPresenterView = loginPresenterView;
    }


    public void callLoginApi( LoginParameter loginParameter ){
    if (((LoginActivity) context).isInternetConneted()) {
        loginPresenterView.onShowloader();
        loginManager.callLoginApi(loginParameter);
    } else {
        loginPresenterView.onError( context.getString( R.string.no_internet_connection ) );
    }
}

    @Override
    public void SuccessLogin( LoginResponse loginResponse ) {
        loginPresenterView.onSucessLogin( loginResponse );
        loginPresenterView.onhideLoader();
    }

    @Override
    public void Error( String errorMessage ) {
        loginPresenterView.onError(errorMessage );
        loginPresenterView.onhideLoader();
    }
}
