package com.walitapp.sales.login;

import android.content.Context;

import com.walitapp.sales.R;
import com.walitapp.sales.basic.Api;
import com.walitapp.sales.login.model.LoginParameter;
import com.walitapp.sales.login.model.LoginResponse;
import com.walitapp.sales.rest.ServiceGenerator;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginManager {
    private LoginManagerCallback loginManagerCallback;
    public Context context;

    public LoginManager( Context context, LoginPresenter loginManagerCallback ) {
        this.loginManagerCallback = loginManagerCallback;
        this.context = context;
    }

    public void callLoginApi( LoginParameter loginParameter ) {
        Api api = ServiceGenerator.createService( Api.class );

        Call<LoginResponse> loginResponseCall = api.callLoginApi(loginParameter);
        loginResponseCall.enqueue( new Callback<LoginResponse>() {
            @Override
            public void onResponse( Call<LoginResponse> call, Response<LoginResponse> response ) {

                if (response.isSuccessful()) {
                    if (response.body().getStatus().equals("success")) {
                        loginManagerCallback.SuccessLogin(response.body());
                    } else {
                        loginManagerCallback.Error(response.body().getMessage());
                    }

                }
            }


            @Override
            public void onFailure( Call<LoginResponse> call, Throwable t ) {
                if(t instanceof IOException){
                    loginManagerCallback.Error(context.getString( R.string.server_down ));
                }else {
                    loginManagerCallback.Error(context.getString( R.string.opps_something_went_wrong ));
                }
            }
        } );
    }


}
