package com.walitapp.sales.login;

import com.walitapp.sales.login.model.LoginResponse;

public interface LoginManagerCallback {
    void SuccessLogin( LoginResponse loginResponse );
    void Error( String errorMessage );

}
