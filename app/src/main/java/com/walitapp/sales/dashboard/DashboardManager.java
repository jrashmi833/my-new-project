package com.walitapp.sales.dashboard;

import android.content.Context;

import com.walitapp.sales.R;
import com.walitapp.sales.basic.Api;
import com.walitapp.sales.dashboard.model.LogoutResponse;
import com.walitapp.sales.rest.ServiceGenerator;
import com.walitapp.sales.utility.SharedPreferenceManager;
import java.io.IOException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardManager {
    private DashboardCallbackListener.DashboardMangerCallBack logoutManagerCallback;
    public Context context;

    public DashboardManager( Context context, DashboardCallbackListener.DashboardMangerCallBack logoutManagerCallback ) {
        this.logoutManagerCallback = logoutManagerCallback;
        this.context = context;
    }

    public void callLogoutApi( ) {
        Api api = ServiceGenerator.createService( Api.class );
        String accessToken = SharedPreferenceManager.getInstance( context ).getUser().getAccessToken();
        Call<LogoutResponse> logoutResponseCall = api.callLogoutApi(accessToken);
        logoutResponseCall.enqueue( new Callback<LogoutResponse>() {
            @Override
            public void onResponse( Call<LogoutResponse> call, Response<LogoutResponse> response ) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus().equals("success")) {
                        logoutManagerCallback.onSuccessLogout(response.body());
                    } else {
                        logoutManagerCallback.onError(response.body().getMessage());
                    }

                }
            }

            @Override
            public void onFailure( Call<LogoutResponse> call, Throwable t ) {
                if(t instanceof IOException){
                    logoutManagerCallback.onError(context.getString( R.string.server_down ));
                }else {
                    logoutManagerCallback.onError(context.getString( R.string.opps_something_went_wrong ));
                }
            }
        } );
    }



}
