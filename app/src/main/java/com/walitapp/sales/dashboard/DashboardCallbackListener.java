package com.walitapp.sales.dashboard;

import com.walitapp.sales.dashboard.model.LogoutResponse;

public interface DashboardCallbackListener {

    public interface DashboardPresenterView {
        void onSuccessLogout( LogoutResponse logoutResponse );
        void onError( String errorMessage );
        void onShowLoader();
        void onTokenChangeError(String message);
        void onHideLoader();
    }

    public interface DashboardMangerCallBack {
        void onSuccessLogout( LogoutResponse logoutResponse );
        void onError( String errorMessage );
        void  onTokenChangeError( String message);
    }
}
