package com.walitapp.sales.dashboard;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.walitapp.sales.Enquiry.EnqiryListFragment;
import com.walitapp.sales.R;
import com.walitapp.sales.basic.BaseActivity;
import com.walitapp.sales.login.LoginActivity;
import com.walitapp.sales.dashboard.model.LogoutResponse;
import com.walitapp.sales.utility.Constant;
import com.walitapp.sales.utility.SharedPreferenceManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DashBoardActivity extends BaseActivity implements DashboardCallbackListener.DashboardPresenterView {


    @BindView(R.id.imageViewlogout)
    ImageView imageViewlogout;
    @BindView(R.id.Enqiry)
    LinearLayout Enqiry;
    private Context context;
    public DashboardPresenter logoutPresenter;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );
        ButterKnife.bind( this );
        logoutPresenter = new DashboardPresenter( this, this );

    }


    public void replaceFragmentHomeFragment( Fragment fragment, String tag, boolean isAddToBackStack ) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add( R.id.relativeLayoutFragmentMainContainer, fragment, tag );
        if (isAddToBackStack) {
            fragmentTransaction.addToBackStack( tag );
        }

        fragmentTransaction.commit();
    }

    @OnClick({R.id.imageViewlogout, R.id.Enqiry})
    public void onViewClicked( View view ) {
        switch (view.getId()) {
            case R.id.imageViewlogout:
//                Intent intent = new Intent( getApplicationContext(), LoginActivity.class );
//                startActivity( intent );
                AlertDialog.Builder a_builder = new AlertDialog.Builder( this );
                a_builder.setMessage( "Are You Sure To Want LogOut" ).setCancelable( false )
                        .setPositiveButton( "Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick( DialogInterface dialog, int which ) {
                                callLogoutApi();

                            }
                        } ).setNegativeButton( "No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick( DialogInterface dialog, int which ) {
                        dialog.cancel();
                    }
                } );
                AlertDialog alert = a_builder.create();
                alert.setTitle( "Alert!!" );
                alert.show();

                break;
            case R.id.Enqiry:
                replaceFragmentHomeFragment( new EnqiryListFragment(), EnqiryListFragment.TAG, true );
                break;
        }
    }

    private void callLogoutApi() {
        logoutPresenter.callLogoutApi();
    }


    @Override
    public void onSuccessLogout( LogoutResponse logoutResponse ) {
        showToast( logoutResponse.getMessage() );
        SharedPreferenceManager.getInstance( context ).putUser( Constant.USER, null );
        SharedPreferenceManager.getInstance( context ).setBoolean( Constant.IS_USER_LOGOUT, false );
        Intent intent = new Intent( this, LoginActivity.class );
        startActivity( intent );
        finish();
    }

    @Override
    public void onError( String errrorMessage ) {
        showToast( errrorMessage );
    }

    @Override
    public void onShowLoader() {
        showLoader();
    }

    @Override
    public void onTokenChangeError( String message ) {
        showToast( message );
    }

    @Override
    public void onHideLoader() {
        hideLoader();

    }
}
