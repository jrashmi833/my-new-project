package com.walitapp.sales.dashboard;

import android.content.Context;
import com.walitapp.sales.R;
import com.walitapp.sales.dashboard.model.LogoutResponse;

public class DashboardPresenter implements DashboardCallbackListener.DashboardMangerCallBack {
    public Context context;
    private DashboardCallbackListener.DashboardPresenterView logoutPresenterView;
    private DashboardManager logoutManager;


    public DashboardPresenter( Context context, DashboardCallbackListener.DashboardPresenterView logoutPresenterView ) {
        this.context = context;
        this.logoutPresenterView =  logoutPresenterView;
        this.logoutManager = new DashboardManager(context,this );
    }


    public void callLogoutApi(){
        if (((DashBoardActivity) context).isInternetConneted()) {
            logoutPresenterView.onShowLoader();
            logoutManager.callLogoutApi();
        } else {
            logoutPresenterView.onError(context.getString( R.string.no_internet_connection ) );
        }}

    @Override
    public void onSuccessLogout( LogoutResponse logoutResponse ) {
        logoutPresenterView.onHideLoader();
        logoutPresenterView.onSuccessLogout( logoutResponse );
    }

    @Override
    public void onError( String errorMessage ) {
        logoutPresenterView.onHideLoader();
        logoutPresenterView.onError( errorMessage );
    }


    @Override
    public void onTokenChangeError( String message ) {
        logoutPresenterView.onHideLoader();
        logoutPresenterView.onTokenChangeError( message );
    }
}
