package com.walitapp.sales.basic;


import com.walitapp.sales.Enquiry.add_enquiry.model.SalesEnquiryParameter;
import com.walitapp.sales.Enquiry.add_enquiry.model.SalesEnquiryResponse;
import com.walitapp.sales.Enquiry.model.EnquiryResponse;
import com.walitapp.sales.login.model.LoginParameter;
import com.walitapp.sales.login.model.LoginResponse;
import com.walitapp.sales.dashboard.model.LogoutResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface Api {
    @POST("SalesLogin")
    Call<LoginResponse> callLoginApi( @Body LoginParameter loginParameter );

    @GET("GetEnquiryList")
    Call<EnquiryResponse> callGetEnquiryListApi( @Header("ACCESS-TOKEN") String token );

    @POST("SalesEnquiry")
    Call<SalesEnquiryResponse> callAddSaleEnquiryApi( @Header("ACCESS-TOKEN") String token, @Body SalesEnquiryParameter salesEnquiryParameter );

    @GET("SalesLogout")
    Call<LogoutResponse> callLogoutApi( @Header("ACCESS-TOKEN") String token );
//    @POST("UserLogin")
//    Call<LoginResponse> callLoginApi( @Body LoginParameter loginParameter );
//
//
//
//    @GET("GetCountryList")
//    Call<com.microvate.newapplication.CountryGroup.model.CountryResponse> callGetCountryGroupApi( @Header("ACCESS-TOKEN") String Tokan );
//
//    @GET("GetCountryList")
//    Call<CountryResponse> callGetCountryApi();
//
//    @GET("GetStateList")
//    Call<StateResponse> callGetStateGroupApi( @Query("country_id") String CountryId );
//
//    @GET("GetStateList")
//    Call<com.microvate.newapplication.TransferData.Model.stateModel.StateResponse> callGetStateApi( @Query("country_id") String CountryId );
//
//    @GET("GetCityList")
//    Call<CityResponse> callGetCityGroupApi( @Query("state_id") String StateId );
//
//    @GET("GetCityList")
//    Call<com.microvate.newapplication.TransferData.Model.citymodel.CityResponse> callGetCityApi( @Query("state_id") String StateId );
//    @GET("GetCallingStatus")
//    Call<CallingResponse> callGetCallingGroupApi();
/*
    @GET("ForgotPassword")
    Call<ForgotPasswordReponse>callForgotApi(@Query("username") String Username);

    @GET("UserLogout")
    Call<LogoutReponse>callLogoutApi( @Header("ACCESS-TOKEN") String Token);*/
}
