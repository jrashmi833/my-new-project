package com.walitapp.sales.basic;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.walitapp.sales.dashboard.DashBoardActivity;
import com.walitapp.sales.R;
import com.walitapp.sales.login.LoginActivity;
import com.walitapp.sales.utility.Constant;
import com.walitapp.sales.utility.SharedPreferenceManager;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_splash );

        new Handler().postDelayed( new Runnable() {
            @Override
            public void run() {
              if(SharedPreferenceManager.getInstance( SplashActivity.this ).getBoolean( Constant.IS_USER_LOGIN ,false)){
                  Intent i = new Intent(SplashActivity.this, DashBoardActivity.class);
                  startActivity(i);
                  finish();
              }else {
                  Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                  startActivity(i);
                  finish();
              }
            }
        },3000 );

    }
}
